const Discord = require('discord.js');
const fs = require('fs');

const { prefix, token } = require('./config.json');
const client = new Discord.Client();

client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles){
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

function statuscheck() {
  if(userVoiceChannel == null) return 0;
  try {
    console.log(userVoiceChannel.members)
  } catch (err) {
    //let's catch, inform about error and log it
    console.log("Something went wrong")
    return console.log(err);
  }
  console.log('set'); // /So I know the timer works
  return statusArray;
}


games = [];
talkingList = {};
trackingID = null;
gameStarted = false;
globalChan = null;
startMic = {};
startGame = null
CODE_SERVER = undefined;
var userVoiceChannel = null;


client.on('guildMemberSpeaking', handleSpeaking.bind(this));
function handleSpeaking(member, speaking) {// Close the writeStream when a member stops speaking
  if(speaking.bitfield == 1){
    startMic[member.user.id] = Date.now();
  }else{
    speakingTime = (Date.now() - startMic[member.user.id])/1000 //speaking time
    console.log(member.user.username, " : ", speakingTime, "s");
    if((speakingTime) > 20000 || isNaN(speakingTime)){
      value = 0;
      console.log("wtf",startMic[member.user.id],Date.now(),speakingTime) 
    }
    if(member.user.username in talkingList){
      _ = talkingList[member.user.username] + Math.round(speakingTime * 100) / 100
      talkingList[member.user.username] = Math.round(_ * 100) / 100
    }else{
      talkingList[member.user.username] = Math.round(speakingTime * 100) / 100
    }
    
  }
  console.log("current list :",talkingList)
}


client.on('message', async message => {
    if(!message.content.startsWith(prefix) || message.author.bot) return; //read only message for bot

    const args = message.content.slice(prefix.length).split(/ +/);
    const command = args.shift().toLowerCase();
    console.log(message.author.username, message.author.id, "tried the command ", command)// this will output the game or any other activity

    if(!client.commands.has(command)) return;
    try {
        client.commands.get(command).execute(message, args);
    }catch(error){
        console.error(error);
        message.reply("Erreur dans la commande");
    }
});

client.on("presenceUpdate", (before, after) => {
  if(trackingID == null || trackingID == undefined || trackingID.id != after.member.id || after.member.presence.activities.filter(x=>x.type === "PLAYING")[0] == undefined) return
  var userActivityBefore = after.member.presence.activities.filter(x=>x.type === "PLAYING")[0]
  var userActivity = after.member.presence.activities.filter(x=>x.type === "PLAYING")[0]
  if(startGame != true && userActivity.name == 'Among Us' && ['In Game','In Freeplay'].includes(userActivity.state)){
    globalChan.reply("You start a game of Among Us")
    talkingList = {"Raptor Jesus" : 5.42};
    startGame = true;
    CODE_SERVER = userActivity.party.id;
    console.log(CODE_SERVER);
  }else if(startGame != false && userActivity.name == 'Among Us' &&  ['In Lobby', 'In Menus'].includes(userActivity.state)){
    startGame = false;
    console.log("ok")
    globalChan.reply("You finished the game")
    var items = Object.keys(talkingList).map(function(key) {      return [key, talkingList[key]];    });
    // Sort the array based on the second element
    items.sort(function(first, second) {      return second[1] - first[1];    });
    games.push(items)
    test = JSON.stringify(items) + " !"
    globalChan.reply(test)
  }else{
    console.log("non", userActivity)
  }
});

client.login(token);